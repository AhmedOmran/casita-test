<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
  integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous">
</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"
  integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous">
</script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"
  integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous">
</script>
<script async defer src="https://maps.googleapis.com/maps/api/js?key={{env('GOOGLE_MAP_KEY')}}&callback=initMap">
</script>

<script>
  var messages = [];
                    var markers = [];

            @foreach ($SortedMessageArray as $Message)
            var temparray = [];
            temparray['sentiment'] = "{{$Message['sentiment']}}";
            temparray['message'] = "{{$Message['message']}}";
            temparray['time'] = "{{$Message['time']}}";
            temparray['message_id'] = "{{$Message['message_id']}}";
            temparray['City'] = "{{$Message['City']}}";
            temparray['Latitude'] = "{{$Message['Location']['Latitude']}}";
            temparray['Longitude'] = "{{$Message['Location']['Longitude']}}";
            messages.push(temparray);
            @endforeach

                    var map;
            function initMap() {
              map = new google.maps.Map(
                  document.getElementById('map'),
                  {center: new google.maps.LatLng(26.820260, 30.578889), zoom: 2});


              var iconBase =
                  'https://developers.google.com/maps/documentation/javascript/examples/full/images/';

              var icons = {
                Neutral: {
                  icon: "{{url('img/map_icon/Neutral.png')}}"
                },
                Negative: {
                  icon: "{{url('img/map_icon/Negative.png')}}"
                },
                Positive: {
                  icon: "{{url('img/map_icon/Positive.png')}}"
                },
                unkown: {
                  icon: "{{url('img/map_icon/unkown.png')}}"
                }

              };
              var features = [

                @foreach ($SortedMessageArray as $Message)
                    {
                    position: new google.maps.LatLng({{$Message['Location']['Latitude']}}, {{$Message['Location']['Longitude']}}),
                    type: '{{$Message['sentiment']}}',
                    message: '{{$Message['message']}}',
                    time: '{{$Message['time']}}',
                    message_id: '{{$Message['message_id']}}',
                    City: '{{$Message['City']}}',
                    Latitude: '{{$Message['Location']['Latitude']}}',
                    Longitude: '{{$Message['Location']['Longitude']}}',

                    },
                @endforeach

              ];

              // Create markers.
              for (var i = 0; i < features.length; i++) {
                var marker = new google.maps.Marker({
                  position: features[i].position,
                  icon: icons[features[i].type].icon,
                  map: map
                });
                markers.push(marker);

              };

              $( ".categoury" ).change(function() {
                for (var i = 0; i < markers.length; i++) {
                        markers[i].setMap(null);
                    }
                    markers = [];
                    $("div.chat_list").remove();

                    for (var i = 0; i < features.length; i++) {


                        if((features[i]['type'] ==  $(".categoury").children("option:selected").val()) || ("All" == $(".categoury").children("option:selected").val())){
                            var marker = new google.maps.Marker({
                        position: features[i].position,
                        icon: icons[features[i].type].icon,
                        map: map
                        });
                        markers.push(marker);

                        $('.inbox_chat').append('<div class=\"chat_list\"><div class=\"chat_people\"><div class=\"chat_img\"><img src=\"http://casita-test.local/img/'+features[i].type+'.png\" alt=\"'+features[i].type+'\"></div><div class=\"chat_ib\"><h5>'+features[i].message+' <span class=\"chat_date\">'+features[i].time+'</span></h5><p>Message ID: '+features[i].message_id+'</p><p>Sentiment: '+features[i].type+'</p><p>City: '+features[i].City+'</p></div></div></div>');

                        }


                    };

            });

            }
</script>