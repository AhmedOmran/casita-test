<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
        integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous" />

    <title>Casita!</title>
    <link rel="stylesheet" type="text/css" href="{{url('style/main.css')}}">

</head>

<body>
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-6" style="    height: 100vh; padding:0px;">
                <div id="map"></div>
            </div>
            <div class="col-sm-6" style="    height: 100vh; padding:0px;">
                <div class="messaging">
                    <div class="inbox_msg">
                        <div class="inbox_people">
                            <div class="headind_srch">
                                <div class="recent_heading">
                                    <form action="" method="get">
                                        <p>
                                            filter By:
                                            <select name="" id="" class="categoury form-control"
                                                style="display: inline !important; width: 80%;">
                                                <option value="All">All</option>
                                                @foreach ($Category as
                                                $CategoryElement)
                                                <option value="{{
                                                            $CategoryElement
                                                        }}">{{
                                                            $CategoryElement
                                                        }}</option>

                                                @endforeach
                                            </select>
                                        </p>
                                    </form>
                                </div>
                            </div>
                            <div class="inbox_chat">
                                @foreach ($SortedMessageArray as $Message)
                                <div class="chat_list">
                                    <div class="chat_people">
                                        <div class="chat_img">
                                            @if ($Message['sentiment'] ==
                                            "Neutral")
                                            <img src="{{
                                                        url('img/Neutral.png')
                                                    }}" alt="Neutral" />
                                            @elseif ($Message['sentiment']
                                            == "Negative")
                                            <img src="{{
                                                        url('img/Negative.png')
                                                    }}" alt="Neutral" />

                                            @elseif ($Message['sentiment']
                                            == "Positive")
                                            <img src="{{
                                                        url('img/Positive.png')
                                                    }}" alt="Neutral" />
                                            @else
                                            <img src="{{
                                                        url('img/unkown.png')
                                                    }}" alt="Neutral" />

                                            @endif
                                        </div>
                                        <div class="chat_ib">
                                            <h5>
                                                {{ $Message["message"] }}
                                                <span class="chat_date">{{
                                                        $Message["time"]
                                                    }}</span>
                                            </h5>
                                            <p>
                                                Message ID:
                                                {{ $Message["message_id"] }}
                                            </p>
                                            <p>
                                                Sentiment:
                                                {{ $Message["sentiment"] }}
                                            </p>
                                            <p>
                                                City: {{ $Message["City"] }}
                                            </p>
                                        </div>
                                    </div>
                                </div>

                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @include('js')
</body>

</html>