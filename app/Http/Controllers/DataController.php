<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Client;

class DataController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //

        $FeedLink = "https://spreadsheets.google.com/feeds/list/0Ai2EnLApq68edEVRNU0xdW9QX1BqQXhHRl9sWDNfQXc/od6/public/basic?alt=json";

        $client = new Client(['verify' => false, 'http_errors' => false]);


        $FeedLinkResult = $client->get($FeedLink, [
            'headers'  => [
                'Accept' => 'application/json',
            ],
        ]);
        $FeedLinkResponse = [
            'body' => $FeedLinkResult->getBody()->getContents(),
            'code' => $FeedLinkResult->getStatusCode(),
            'ReasonPhrase' => $FeedLinkResult->getReasonPhrase(),
        ];
        if ($FeedLinkResponse['code'] == 200) {
            $FeedLinkData =  json_decode($FeedLinkResponse['body'], TRUE);

            $FilteredData = array();
            $FilteredData['last_update'] = array_values($FeedLinkData['feed']['updated'])[0];

            $MessageArray = array();

            foreach ($FeedLinkData['feed']['entry'] as $key => $value) {
                $TempArray = array();

                $TempArray['data_feed_link'] = $value["id"]["\$t"];
                $TempArray['time'] = $value["title"]["\$t"];
                $TempArray['time'] = \Carbon\Carbon::createFromIsoFormat('MM-DD-YY H:m', $TempArray['time']);
                $TempArrayData = explode(",", $value["content"]["\$t"]);



                $MessageID = explode(',', $value["content"]["\$t"], 2);
                $TempArray['message_id'] = array_map('trim', explode(":", $MessageID[0]))[1];

                $MessageTempHolder = trim(implode(",", array_slice($TempArrayData, 1, -1)));
                $TempArray['message'] = array_map('trim', explode(":", $MessageTempHolder))[1];

                $reversedParts = explode(',', strrev($value["content"]["\$t"]), 2);

                $partThree = strrev($reversedParts[0]);
                $partThree = array_map('trim', explode(":", $partThree))[1];

                $TempArray['sentiment'] = $partThree;

                $AnlayzePhraseForCity = $client->request('POST', "https://language.googleapis.com/v1beta2/documents:analyzeEntities?key=" . env('GOOGLE_COMPUTE_CODE') . "", [
                    'body' => "{
                        \"document\": {
                            \"type\": \"PLAIN_TEXT\",
                             \"language\": \"en\",
                            \"content\": \"" . $TempArray['message'] . "\"
                        }
                      }
                      ",
                    'headers' => [
                        'Content-Type'     => 'application/json',
                    ]
                ]);

                $AnlayzePhraseForCityResponse = [
                    'body' => $AnlayzePhraseForCity->getBody()->getContents(),
                    'code' => $AnlayzePhraseForCity->getStatusCode(),
                    'ReasonPhrase' => $AnlayzePhraseForCity->getReasonPhrase(),
                ];

                $PhraseEntities =  json_decode($AnlayzePhraseForCityResponse['body'], true)['entities'];

                foreach ($PhraseEntities as $key => $value) {
                    if ($value['type'] == "LOCATION" && count($value['metadata']) >= 1) {
                        $TempArray['City'] = $value['name'];
                        break;
                    }
                }


                $CoordintesForCity = $client->get('https://geocoder.api.here.com/6.2/geocode.json?app_id=' . env('HERE_APP_ID') . '&app_code=' . env('HERE_APP_CODE') . '&searchtext=' . $TempArray['City'] . '', [
                    'headers'  => [
                        'Accept' => 'application/json',
                    ],
                ]);
                $CoordintesForCityResponse = [
                    'body' => $CoordintesForCity->getBody()->getContents(),
                    'code' => $CoordintesForCity->getStatusCode(),
                    'ReasonPhrase' => $CoordintesForCity->getReasonPhrase(),
                ];
                $Location = json_decode($CoordintesForCityResponse['body'], true)['Response']['View'][0]['Result'][0]['Location']['DisplayPosition'];
                $TempArray['Location'] = $Location;

                $MessageArray[] = $TempArray;
            }
        }

        $Category = array_unique(array_column($MessageArray, 'sentiment'));

        $SortedMessageArray = array();

        foreach ($Category as $CategoryKey => $CategoryValue) {
            foreach ($MessageArray as $MessageArraykey => $MessageArrayValue) {
                if ($MessageArrayValue['sentiment'] == $CategoryValue) {
                    $SortedMessageArray[] = $MessageArrayValue;
                }
            }
        }


        return view('index', compact('SortedMessageArray', 'Category'));
    }
}
